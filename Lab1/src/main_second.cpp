#include "rng.h"

#include <cxxopts.hpp>

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <vector>

template<typename Iterator>
void bubble_sort(Iterator begin, Iterator end) {
  size_t size = end - begin;
  for (size_t i = 0; i < size; ++i) {
    for (Iterator it = end - 1; it > begin + i; --it) {
      if (*it < *(it - 1)) {
        std::swap(*it, *(it - 1));
      }
    }
  }
}

template<typename Iterator>
void insertion_sort(Iterator begin, Iterator end) {
  for (Iterator it = begin + 1; it < end; ++it) {
    for (Iterator x = it; x > begin && *x < *(x - 1); --x) {
      std::swap(*x, *(x - 1));
    }
  }
}

#define UNUSED(X) ((void)(X))

using Clock = std::chrono::system_clock;

template<typename Func>
Clock::duration run_x_times_and_measure_average_time(Func&& func, int times) {
  Clock::duration sum(0);
  for (int i = 0; i < times; ++i) {
    sum += func();
  }
  return sum / times;
}

template<typename Func>
Clock::duration sort_with_time_measure(Func&& sort, std::vector<unsigned long long> vector) {
  RandomNumberGenerator rng;
  std::generate(vector.begin(), vector.end(), [&](){ return rng.getNext(); });

  const auto start = Clock::now();
  sort(vector.begin(), vector.end());
  const auto elapsed = Clock::now() - start;

  // Mark all vector unused to avoid optimisation
  std::for_each(vector.begin(), vector.end(), [](auto x){ UNUSED(x); });
  return elapsed;
}

void run(unsigned int count_from, unsigned int count_to) {
  std::cout << "Bubble sort" << std::endl;
  std::cout << "Size\tTime ms" << std::endl;
  for (unsigned int count = count_from; count <= count_to; count *= 2) {
    auto bubble_sort_avg_duration = run_x_times_and_measure_average_time([=](){
        auto sort_func = bubble_sort<std::vector<unsigned long long>::iterator>;
        std::vector<unsigned long long> vector(count);
        return sort_with_time_measure(sort_func, vector);
    }, 10);

    long long bubble_sort_avg_duration_milliseconds
      = std::chrono::duration_cast<std::chrono::milliseconds>(bubble_sort_avg_duration).count();
    std::cout << count << "\t" << bubble_sort_avg_duration_milliseconds << std::endl;
  }

  std::cout << std::endl << "Insertion sort" << std::endl;
  std::cout << "Size\tTime ms" << std::endl;

  for (unsigned int count = count_from; count <= count_to; count *= 2) {
    auto insertion_sort_avg_duration = run_x_times_and_measure_average_time([=](){
        auto sort_func = insertion_sort<std::vector<unsigned long long>::iterator>;
        std::vector<unsigned long long> vector(count);
        return sort_with_time_measure(sort_func, vector);
    }, 10);

    long long insertion_sort_avg_duration_milliseconds
      = std::chrono::duration_cast<std::chrono::milliseconds>(insertion_sort_avg_duration).count();
    std::cout << count << "\t" << insertion_sort_avg_duration_milliseconds << std::endl;
  }
}

int main(int argc, char** argv) {
  cxxopts::Options options("", "First lab");
  options.add_options()
    ("from", "Start vector size", cxxopts::value<unsigned int>())
    ("to", "End vector size", cxxopts::value<unsigned int>())
    ("h,help", "Show help");

  try {
    auto result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help({""}) << std::endl;
      return 0;
    }

    if (!result.count("from") || !result.count("to")) {
      std::cerr << "Params required!" << std::endl;
      std::cout << options.help({""}) << std::endl;
      return 1;
    }

    unsigned int from = result["from"].as<unsigned int>();
    unsigned int to = result["to"].as<unsigned int>();
    run(from, to);
  } catch (const cxxopts::OptionException& e) {
    std::cerr << "Error parsing options: " << e.what() << std::endl;
    std::cout << options.help({""}) << std::endl;
    return 1;
  }
}
