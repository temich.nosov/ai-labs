#pragma once

class RandomNumberGenerator {
private:
  unsigned long long u;
  unsigned long long v;
  unsigned long long w;

public:
  RandomNumberGenerator();
  RandomNumberGenerator(unsigned long long seed);

  unsigned long long getNext();
  double getNextDouble();
};
