#include "rng.h"

#include <cxxopts.hpp>
#include <iomanip>
#include <iostream>

void run(unsigned int count, RandomNumberGenerator&& rng) {
  double sum = 0;
  double sum_of_squares = 0;
  for (unsigned int i = 0; i < count; ++i) {
    double cur = rng.getNextDouble();
    sum += cur;
    sum_of_squares += cur * cur;
  }

  const double expectation = sum / count;
  const double variance = (sum_of_squares / count) - expectation * expecration;

  std::cout << std::fixed << std::setprecision(5);
  std::cout << "Expectation is " << expecration << std::endl;
  std::cout << "Variance is " << variance << std::endl;
}

int main(int argc, char** argv) {
  try {
    cxxopts::Options options("RandonNumberGenerator", "First lab");
    options.add_options()
      ("n,count", "Count of number to generate", cxxopts::value<unsigned int>())
      ("s,seed",
       "Seed for random number generator. Omit to use current timestamp.",
       cxxopts::value<unsigned long long>())
      ("h,help", "Show help")
      ;

    auto result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help({""}) << std::endl;
      return 0;
    }

    if (!result.count("count")) {
      std::cerr << "Count required!" << std::endl;
      return 1;
    }

    unsigned int count = result["count"].as<unsigned int>();

    if (result.count("seed")) {
      unsigned long long seed = result["seed"].as<unsigned long long>();
      run(count, RandomNumberGenerator(seed));
    } else {
      run(count, RandomNumberGenerator());
    }
  } catch (const cxxopts::OptionException& e) {
    std::cerr << "Error parsing options: " << e.what() << std::endl;
    return 1;
  }
}
