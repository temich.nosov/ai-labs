#include "rng.h"

#include <chrono>

static unsigned long long getTimestamp() {
  return std::chrono::duration_cast<std::chrono::microseconds>(
    std::chrono::system_clock::now().time_since_epoch()
  ).count();
}

RandomNumberGenerator::RandomNumberGenerator() : RandomNumberGenerator(getTimestamp()) {}
RandomNumberGenerator::RandomNumberGenerator(unsigned long long seed)
  : v(4101842887655102017LL), w(1)
{
  u = seed ^ v;
  getNext();

  v = u;
  getNext();

  w = v;
  getNext();
}

unsigned long long RandomNumberGenerator::getNext() {
  u = u * 2862933555777941757LL + 7046029254386353087LL;
  v ^= v >> 17;
  v ^= v << 31;
  v ^= v >> 8;
  w = 4294957665U * (w & 0xffffffff) + (w >> 32);
  unsigned long long x = u ^ (u << 21);
  x ^= x >> 35;
  x ^= x << 4;
  return (x + v) ^ w;
}

double RandomNumberGenerator::getNextDouble() {
  return 5.42101086242752217E-20 * getNext();
}
