# Лаборатрная работа #1
## Сборка
Для сборки использовался `cmake` версии 3.13.4 и компилятор `clang` версии 6.0.0.
Сборка и запуск могут быть осуществлены следующим образом:
```bash
mkdir build
cd build
cmake ..
make
./first_part --help
./second_part --help
```

## 1. Генератор случайных чисел
Генератор случайных чисел находится в файлах `rng.cpp` и `rng.h`
Для проверки генератора написан main'ик в файлике `main.cpp`

Пример запуска:
```console
foo@bar:~$ ./first_part -n 1000 -s 0
Expectation is 0.50063
Variance is 0.08361
foo@bar:~$ ./first_part -n 10000000 -s 0
Expectation is 0.50009
Variance is 0.08334
```

## 2. Алгоритмы сортировки
Main'ик для второй части находится в файле `main_second.cpp`
Результаты запуска:
```console
foo@bar:~$ ./second_part --from 1000 --to 128000
Bubble sort
Size    Time ms
1000    0
2000    2
4000    9
8000    53
16000   258
32000   1086
64000   5199
128000  20323

Insertion sort
Size    Time ms
1000    0
2000    0
4000    2
8000    8
16000   36
32000   152
64000   646
128000  2684
```

Эти же результаты в виде таблички:

Итераций | Время insertion sort | Время bubble sort
--- | --- | ---
1000 | 0 | 0
2000 | 0 | 0.002
4000 | 0.002 | 0.009
8000 | 0.008 | 0.053
16000 | 0.036 | 0.258
32000 | 0.152 | 1.086
64000 | 0.646 | 5.199
128000 | 2.684 | 20.323

И на графике:

![График зависимости времени выполнения от размера](graph.png)
